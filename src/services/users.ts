import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}
const saveUser = (User: User) => {
  return http.post("/users", User);
};

const updateUser = (id: number, User: User) => {
  return http.patch(`/users/${id}`, User);
};
const deleteUser = (id: number) => {
  return http.delete(`/users/${id}`);
};

export default { deleteUser, updateUser, getUsers, saveUser };
