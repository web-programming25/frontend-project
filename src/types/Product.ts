export default interface Product {
  id?: number;
  name: string;
  price: number;
  createdAT?: Date;
  updatedAT?: Date;
  deletedAT?: Date;
}
