import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/users";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const editUser = ref<User>({ name: "", login: "", password: "" });
  const users = ref<User[]>([]);
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editUser.value = { name: "", login: "", password: "" };
    }
  });
  const getUser = async () => {
    loadingStore.isLoading = true;

    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Cannot save User");
    }
    loadingStore.isLoading = false;
  };
  const saveUser = async () => {
    loadingStore.isLoading = true;

    if (editUser.value.id) {
      try {
        dialog.value = false;
        const res = await userService.updateUser(
          editUser.value.id,
          editUser.value
        );
        await getUser();
      } catch (err) {
        console.log(err);
        messageStore.showError("Cannot save User");
      }
    } else {
      try {
        dialog.value = false;
        const res = await userService.saveUser(editUser.value);
        await getUser();
      } catch (e) {
        console.log(e);
        messageStore.showError("Cannot save User");
      }
    }
    loadingStore.isLoading = false;
  };
  const EditUser = (user: User) => {
    editUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  };

  const deleteUser = async (id: number) => {
    loadingStore.isLoading = true;

    try {
      const res = await userService.deleteUser(id);
      await getUser();
    } catch (err) {
      console.log(err);
      messageStore.showError("Cannot save User");
    }
    loadingStore.isLoading = false;
  };

  return { deleteUser, EditUser, dialog, editUser, getUser, users, saveUser };
});
